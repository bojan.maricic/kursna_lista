<?php include('db.php'); ?>

<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Kursna lista</title>	
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Kalam|Roboto+Condensed:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
	</head>
	
	<body>
			<!--KONTEJNER-->
			<div class="container">
				
				<!--KONTAKT-->
					<h1>Konvertor valuta</h1>
					<div class="posalji-poruku">
						<form action="convert.php" method="POST" enctype="multipart/form-data" id="form">
							<p>Unesi iznos za konverziju</p>
							<input type="text" name="iznos" class="kontakt-forma-polje" required="required" >
							<p>Izaberi valutu</p>
							<select type="text" name="valuta" class="kontakt-forma-polje" value="" required="required">
								  <option value="">Izaberi valutu</option>
								  <?php

								  $sql = "SELECT * FROM valuta ORDER BY naziv ASC";
								  $result = mysqli_query($connection,$sql) or die(mysql_error());

								  if (mysqli_num_rows($result)>0) {
									
									while ($record = mysqli_fetch_array($result,MYSQLI_BOTH)){
									  echo "<option value=\"$record[id]\">$record[naziv]</option>";         
									}
								  }
								  ?>
							</select>
							<p>Izaberi kurs</p>
							<input type="radio" name="kurs" value="kupovni" id="kupK" checked> Kupovni
							<input type="radio" name="kurs" value="srednji" id="sredK"> Srednji
							<input type="radio" name="kurs" value="prodajni" id="prodK"> Prodajni
	
							<p><button type="submit" name="convert" id="convert">Pošalji</button><p>
						</form>
					</div>
					<div class="link">
					<p><a href="create_json_file.php" id="link">Izlistaj JSON</a></p>
					</div>
					<div class="images">
					<img src="images/money.png">
					</div>
					
					
					
				<!--KRAJ KONTAKTA-->
			
			</div>
			<!--KRAJ KONTEJNERA-->
	</body>
</html>