-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 18, 2018 at 10:48 AM
-- Server version: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kursna_lista`
--

-- --------------------------------------------------------

--
-- Table structure for table `konvertovana_valuta`
--

DROP TABLE IF EXISTS `konvertovana_valuta`;
CREATE TABLE IF NOT EXISTS `konvertovana_valuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `iznos` float NOT NULL,
  `valuta` varchar(20) CHARACTER SET utf8 NOT NULL,
  `iznos_kursa` float NOT NULL,
  `konvertovani_iznos` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konvertovana_valuta`
--

INSERT INTO `konvertovana_valuta` (`id`, `datum`, `iznos`, `valuta`, `iznos_kursa`, `konvertovani_iznos`) VALUES
(15, '2018-06-18', 200, 'euro', 118.9, 23780),
(14, '2018-06-18', 200, 'euro', 118.9, 23780),
(13, '2018-06-18', 200, 'euro', 118.9, 23780),
(12, '2018-06-18', 200, 'euro', 118.9, 23780),
(11, '2018-06-18', 200, 'euro', 118.9, 23780),
(10, '2018-06-18', 200, 'euro', 118.9, 23780),
(9, '2018-06-18', 200, 'euro', 118.9, 23780),
(16, '2018-06-18', 200, 'euro', 118.9, 23780),
(17, '2018-06-18', 200, 'euro', 118.9, 23780),
(18, '2018-06-18', 200, 'euro', 118.9, 23780),
(19, '2018-06-18', 200, 'euro', 118.9, 23780),
(20, '2018-06-18', 200, 'euro', 118.9, 23780),
(21, '2018-06-18', 200, 'euro', 118.9, 23780),
(22, '2018-06-18', 200, 'euro', 118.9, 23780),
(23, '2018-06-18', 200, 'euro', 118.9, 23780),
(24, '2018-06-18', 200, 'euro', 118.9, 23780),
(25, '2018-06-18', 200, 'euro', 118.9, 23780),
(26, '2018-06-18', 100, 'Kanadski dolar', 75, 7500),
(27, '2018-06-18', 100, 'Kanadski dolar', 75, 7500),
(28, '2018-06-18', 200, 'Britanska funta', 131.2, 26240);

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

DROP TABLE IF EXISTS `kurs_dinara`;
CREATE TABLE IF NOT EXISTS `kurs_dinara` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `valuta` int(5) NOT NULL,
  `kupovni` float NOT NULL,
  `prodajni` float NOT NULL,
  `srednji` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`ID`, `datum`, `valuta`, `kupovni`, `prodajni`, `srednji`) VALUES
(1, '2018-06-18', 1, 117.5, 118.9, 118.175),
(2, '2018-06-18', 2, 99.7, 102.65, 102.1),
(3, '2018-06-18', 3, 99.5, 102.95, 102.218),
(4, '2018-06-18', 4, 131.2, 138.54, 135.227),
(5, '2018-06-18', 5, 73.56, 77.35, 75.84),
(6, '2018-06-18', 6, 75, 78.86, 77.329),
(7, '2018-06-18', 7, 11.22, 11.58, 11.8),
(8, '2018-06-18', 8, 15.37, 15.855, 16.17),
(9, '2018-06-14', 9, 12.11, 12.869, 12.492);

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(20) CHARACTER SET utf8 NOT NULL,
  `kod` varchar(5) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'euro', 'eur'),
(2, 'Američki dolar', 'usd'),
(3, 'Švajcarski franak', 'chf'),
(4, 'Britanska funta', 'gbp'),
(5, 'Australijski dolar', 'aud'),
(6, 'Kanadski dolar', 'cad'),
(7, 'Švedska kruna', 'sek'),
(8, 'Danska kruna', 'dkk'),
(9, 'Norveška kruna', 'nok');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
